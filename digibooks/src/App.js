import './Library.css';
import Header from "./Components/Header";
import LogIn from "./Components/LogIn";
import SignUp from "./Components/SignUp";
import SideImage from "./Components/SideImage";

import { useState } from "react";
import { Routes, Route } from 'react-router-dom'


function App() {
  const [isMobile, setIsMobile] = useState(true)
  const [signUp, setSignUp] = useState(false)
  //const [loggedIn, setLoggedIn] = useState(true)


  const checkWindowSize = ()=>{
    if (window.innerWidth <= 500)
    {
      setIsMobile(true);
    }
    else 
    {
      setIsMobile(false);
    }   
  }

  window.addEventListener('resize', checkWindowSize);

  return (
    <div className={isMobile ? "App-mobile" : "App-web"}>
      {isMobile === true && 
      <Header isMobile={isMobile}/>}
     
      {signUp === false &&
        <LogIn 
          isMobile={isMobile}
          signUp={signUp}
          setSignUp={setSignUp}
      />} 

      {signUp === true && 
        <SignUp 
          isMobile={isMobile}
          signUp={signUp}
          setSignUp={setSignUp}
      />}


      {isMobile === false && <SideImage />} 

    </div>
  );
}

export default App;
