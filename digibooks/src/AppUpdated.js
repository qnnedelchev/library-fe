import './Library.css';
import Header from "./Components/Header";
import LogIn from "./Components/LogIn";
import SignUp from "./Components/SignUp";
import SideImage from "./Components/SideImage";
import Library from './Components/Library';
import BookPage from './Components/BookPage';

import { useState, useEffect } from "react";
import {Route, Routes } from 'react-router-dom'
import Layout from './Components/Layout';

function AppUpdated() {
  const [isMobile, setIsMobile] = useState(true)
  const [signUp, setSignUp] = useState(false)
  const [loggedIn, setLoggedIn] = useState(false)
  const [accessToken, setAccessToken] = useState('')
  const [book, setBook] = useState('')

  const checkWindowSize = ()=>{
    if (window.innerWidth <= 500)
    {
      setIsMobile(true);
    }
    else 
    {
      setIsMobile(false);
    }   
  }

  window.addEventListener('resize', checkWindowSize);

  return (
   
    <Routes>
      <Route path="/"  element={<Layout isMobile={isMobile} loggedIn={loggedIn}/>}>
        <Route path="/" element={
          <LogIn 
          isMobile={isMobile}
          signUp={signUp}
          setSignUp={setSignUp}
          setLoggedIn={setLoggedIn}
          setAccessToken={setAccessToken}
          />
        }>
        </Route> 
        <Route path="/register" element={
          <SignUp 
            isMobile={isMobile}
            signUp={signUp}
            setSignUp={setSignUp}
          />
        }>
        </Route>
        <Route path="/books" element={ <Library accessToken={accessToken} setBook={setBook}/> }>
        </Route>
        <Route path="/books/:id" element={ <BookPage book={book}/> }>
        </Route>
      </Route>
    </Routes>
  );
}

export default AppUpdated;
