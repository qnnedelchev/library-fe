
import polygon from '../images/polygon.svg'
import { Link, useNavigate, useLocation} from 'react-router-dom';

const BookItem = ({book, setBook}) => {

  const DATE = /[0-9]*-[0-9]*-[0-9]*/i;

  const navigate = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathname || '/';

  const openDetails = () => {
    setBook(book)
    navigate(`/books/${book._id}`, {replace: true});
  }

  return (
    <li className="book-container">
        <img className="book-cover" src={book.image} alt={book.name}></img>
        <h2 className="book-title">{book.name}</h2>
        <p className="book-author">{book.author}</p>
        <p className="book-genre">Genre: <b>{book.genre.name}</b></p>
        <div className="book-dates">
            <p className='created'>Created on: <b>{book.createOn.match(DATE)}</b></p>
            <p className='updated'>Updated on: <b>{book.lastUpdateOn.match(DATE)}</b></p>
        </div>
        <button className="show-book" onClick={openDetails}>
            <img src={polygon} alt="Show Book"></img>
        </button>
    </li>
  )
}

export default BookItem