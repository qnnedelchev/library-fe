import BookItem from "./BookItem"
const BookList = ({books, isMobile, setBook}) => {
  return (
    <ul className={isMobile ? '' : "books"}>
        {books.map((book) => (
            <BookItem 
            key={book._id}
            book={book}
            setBook={setBook}
            />
        ))}
    </ul>
  )
}

export default BookList