import { useState, useEffect } from "react";
import LibraryHeader from "./LibraryHeader";
import backArrow from '../images/Back_arrow.svg'

const BookPage = ({book}) => {

  const backToLibrary = () => {
    console.log('BACK')
  }   




  const DATE = /[0-9]*-[0-9]*-[0-9]*/i;

  return (
    <main>
        <LibraryHeader />
        <nav className="nav-back">
            <img className="back-arrow" src={backArrow} alt="Back to Library" onClick={backToLibrary}></img>
            <p>Library</p>
        </nav>
        <div className="book-cover">
            <img className="book" src={book.image} alt="Book front cover"></img>
        </div>
        <div className="details">
            <h1 className="title">{book.name}</h1>
            <h2 className="author">{book.author}</h2>
            <p className="genre"><b>{book.genre}</b></p>
            <div className="book-dates">
                <p className='created'>Created on: <b>{book.createOn.match(DATE)}</b></p>
                <p className='updated'>Updated on: <b>{book.lastUpdateOn.match(DATE)}</b></p>
            </div>
            <h2 className="description">Short description</h2>
            <p></p>
        </div>
    </main>
  )
}

export default BookPage