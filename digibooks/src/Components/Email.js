import { useForm } from 'react-hook-form';
const Email = () => {

  const { handleSubmit } = useForm();

  const onSubmit = (data) =>
  {
    console.log(data);
  }

  return (
    <form className="emailForm" onSubmit={handleSubmit(onSubmit)}>
          <label htmlFor="email">Email</label>
            <input
            autoFocus
            className="emailInput"
            name='email'
            id='email'
            type='email'
            required
            />
      </form>
  )
}

export default Email