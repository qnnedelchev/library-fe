const Footer = () => {
  return (
    <footer>
      <p>You don't have an account? <b>Sign in here</b></p>
    </footer>
  )
}

export default Footer