import logo from '../images/logo.png'
const Header = ({ isMobile }) => {
  return (
    <header className={isMobile ? 'header-mobile' : 'header-web'}>
        <img className="logo" src={logo} alt="DigiBooks green logo"></img>
    </header>
  )
}

export default Header