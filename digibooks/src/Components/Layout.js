import { Outlet } from 'react-router-dom'
import Header from "./Header";
import SideImage from "./SideImage";
const Layout = ({isMobile, loggedIn}) => {
  return (
    <div className={!loggedIn ? (isMobile ? "App-mobile" : "App-web") : ''}>
      {isMobile === true && !loggedIn &&
      <Header isMobile={isMobile}/>}
      
      <Outlet />

      {isMobile === false && !loggedIn && <SideImage />} 
    </div>
  )
}

export default Layout