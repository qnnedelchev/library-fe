import { useState, useEffect } from "react";
import '../Library.css';
import LibraryHeader from "./LibraryHeader";
import Search from "./Search";
import BookList from "./BookList";

function Library({ accessToken, setBook }) {
  const API_URL = "https://books-library-dev.herokuapp.com"

  const [isMobile, setIsMobile] = useState(true)
  const [books, setBooks] = useState([]);
  const [fetchError, setFetchError] = useState(null);

  const checkWindowSize = ()=>{
    if (window.innerWidth <= 500)
    {
      setIsMobile(true);
    }
    else 
    {
      setIsMobile(false);
    }   
  }

  window.addEventListener('resize', checkWindowSize);


  useEffect(() => {

    const fetchItems = async () => {

      const getOptions = {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${accessToken}` 
        }
      }
      try{
        const response = await fetch(`https://books-library-dev.herokuapp.com/api/book`, getOptions);
        if(!response.ok) throw Error('Did not receive expected data');
  
        const listBooks = await response.json();
        setBooks(listBooks);
        
        setFetchError(null);
      } catch (err) {
        setFetchError(err.message);
      }
    }
    fetchItems();
  }, [])


  return (
    <div className={isMobile ? "Library-mobile" : "Library-web"}>
      <LibraryHeader isMobile={isMobile}/>
      <div className={isMobile ? "library-container-mobile" : "library-container-web"}>
        <Search />
        {fetchError !== null && <p>{fetchError}</p>}
        <BookList 
          books={books}
          isMobile={isMobile}
          setBook={setBook}
        />
      </div>
    </div>
  );
}

export default Library;