import logo from '../images/logo.png'
import menu from '../images/menu.svg'
import profile from '../images/profile.svg'

const LibraryHeader = ({isMobile}) => {
  return (
      <header>
        {isMobile && 
          <div className='library-header-mobile' >
            <img className="menu" src={menu} alt="Menu Button"></img>
            <img className="logo-library" src={logo} alt="DigiBooks green logo"></img>
            <img className="profile" src={profile} alt="Profile button"></img>
          </div>}

          {!isMobile && 
            <div className='library-header-web' >
              <img className="logo-library" src={logo} alt="DigiBooks green logo"></img>
              <div className='menu'>
                <p className='library'>Library</p>
                <p className='settings'>Settings</p>
              </div>
              <img className="profile" src={profile} alt="Profile button"></img>
            </div>}

        </header>   
  )
}

export default LibraryHeader