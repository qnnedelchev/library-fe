import Header from './Header'

import viewEmpty from '../images/viewEmpty.png';
import view from '../images/view.svg';

import { useState, useRef, useEffect } from 'react';
import { Link, useNavigate, useLocation} from 'react-router-dom';

import useAuth from '../hooks/useAuth';
import axios from '../api/axios'

//CONSTS
//const USER_REGEX = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
//const PWD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{8,24}$/;

const USER_REGEX = /[a-zA-Z0-9_]{4,}$/i;
const PWD_REGEX = /([a-zA-Z0-9]){6,}$/;
const REGISTER_URL = '/api/user/register'
const BASE_URL = "https://books-library-dev.herokuapp.com";

const LogIn = ({ isMobile, signUp, setSignUp, setLoggedIn, setAccessToken}) => {

const { setAuth } = useAuth();

const navigate = useNavigate();
const location = useLocation();
const from = location.state?.from?.pathname || '/';

const userRef = useRef();
const errRef = useRef();

const [user, setUser] = useState('');
const [validName, setValidName] = useState(false);
const [userFocus, setUserFocus] = useState(false);

const [pwd, setPwd] = useState('');
const [validPwd, setValidPwd] = useState(false);
const [pwdFocus, setPwdFocus] = useState(false);

const [showPassword,setShowPassword] = useState(false)

const [errMsg, setErrMsg] = useState('');
const [success, setSuccess] = useState('');

useEffect(() =>{
    userRef.current.focus();
  }, [])
  
useEffect(() => {
    const result = USER_REGEX.test(user);
    console.log(result);
    console.log(user);
    setValidName(result);
}, [user])

useEffect(() => {
    const result = PWD_REGEX.test(pwd);
    console.log(result)
    console.log(pwd)
    setValidPwd(result);
}, [pwd])

useEffect(() => {
    setErrMsg('')
  }, [user, pwd])


const handleSignUp = () =>{
    setSignUp(!signUp);
  }

const handleSubmit = async (e) =>{
    e.preventDefault();
    

    const v1 = USER_REGEX.test(user);
    const v2 = PWD_REGEX.test(pwd);
    if(!v1 || !v2){
        setErrMsg("Invalid Entry");
        return;
    }
    console.log('SUBMIT')
    try {
            const response = await axios.post('/api/user/login', 
                JSON.stringify({username: user, password: pwd}),
                {
                    headers: { 'Content-Type': 'application/json'}
                }
                
            );
            console.log(JSON.stringify(response?.data));
            const token = response?.data?.token;
            setAuth({user, pwd, token});
            setUser('');
            setPwd('');
            setLoggedIn(true);
            setAccessToken(token);
            navigate('/books', {replace: true});
    }catch (err) {
        if(!err?.response){
            setErrMsg('No Server Response');
            console.log('No Server Response')
        }else if (err.response?.status === 409){
            setErrMsg('Username already used');
            console.log('Username already used');
        }else if (err.response?.status === 401){
            setErrMsg('Unauthorize');
            console.log('Unauthorized');
        }else {
            setErrMsg('Log In Failed');
            console.log('Log In failed');
        }
    }
}
 
function handlePassword(){
    setShowPassword(!showPassword);
}

return (
    <>
    {success ? (
        <section>
            <h1>Success!</h1>
        </section>) : (
    <form onSubmit={handleSubmit} className={isMobile ? "logInForm-mobile" : "logInForm-web"} >
        {isMobile ? '' : <Header isMobile={isMobile}/>}
        <h2 className='title-mobile'>
        Welcome back!
      </h2>
        <div className="emailForm">
                <label htmlFor="email">Username</label>
                <input
                className="emailInput"
                id='username'
                type='text'
                ref={userRef}
                required
                autoComplete='off'
                onChange={(e) => setUser(e.target.value)}
                value={user}
                onFocus={() => setUserFocus(true)}
                onBlur={() => setUserFocus(false)}
                />
            </div>
    
            <div className="passwordForm">
                <label htmlFor="password">Password</label>
                <div className="passwordContainer">
                    <input
                    className="passwordInput" 
                    id='password'
                    type={showPassword ? 'text' : 'password'}
                    required
                    onChange={(e) => setPwd(e.target.value)}
                    value={pwd}
                    onFocus={() => setPwdFocus(true)}
                    onBlur={() => setPwdFocus(false)}
                    />
                    <img src={showPassword ? view : viewEmpty} alt="Show Password" onClick={handlePassword}></img>
                </div>

                <button
                    type="submit"
                    aria-label="Log In"
                    className={!validName || !validPwd  ? "btn-mobile unactive" : "btn-mobile active"}
                    disabled={!validName || !validPwd ? true : false}
                >
                Log In
                </button>
            </div>

            <footer>
                <p>You don't have an account? <Link to="/register"><b className='blue' onClick={handleSignUp}> Sign Up Here</b></Link></p>
            </footer>

    </form> )}
</>
)
}

export default LogIn