import Title from './Title'
import Header from './Header'
import Footer from './Footer'

import viewEmpty from '../images/viewEmpty.png';
import view from '../images/view.svg';

import { useState } from 'react';
import { useForm } from 'react-hook-form';

const LogInForm = ({ isMobile }) => {

const { handleSubmit, register } = useForm();
const [validations,setValidations] = useState([])
const [strength,setStrength] = useState(0)
const [showPassword,setShowPassword] = useState(false)


const onSubmit = (data) =>
{
      console.log(data.name);
}

function validatePassword(e){
  setValidations(
    [
      (e.target.value.length > 5),
      (e.target.value.search(/[A-Z]/) > -1),
      (e.target.value.search(/[0-9]/) > -1),
      (e.target.value.search(/[$&+,:;=?@#]/) > -1)
    ]
  );

  setStrength(validations.reduce((acc,cur)=> acc+cur))
}

function handlePassword(){
  setShowPassword(!showPassword);
}

return (
  <form className={isMobile ? "logInForm-mobile" : "logInForm-web"} onSubmit={handleSubmit(onSubmit)}>
      {isMobile ? '' : <Header isMobile={isMobile}/>}
      <Title />
  <div className="emailForm">
        <label htmlFor="email">Email</label>
          <input
          autoFocus
          className="emailInput"
          name='email'
       
          id='email'
          type='email'
          />
    </div>

    <div className="passwordForm">
        <label htmlFor="password">Password</label>
          <div className="passwordContainer">
            <input
            autoFocus
            className="passwordInput"
            id='password'
            type={showPassword ? 'text' : 'password'}
            required
            onChange={validatePassword}
            />
            <img src={showPassword ? view : viewEmpty} alt="Show Password" onClick={handlePassword}></img>
          </div>
          
          <p className="smallTxt">Recover password</p>
          <button
          type="submit"
          aria-label="Log In"
          className={strength >= 3 ? "btn-mobile nactive" : "btn-mobile unactive"}
          >
          Log In
          </button>
    </div>

    <Footer />
  </form>
)
}

export default LogInForm