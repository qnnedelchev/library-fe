import viewEmpty from '../images/viewEmpty.png';
import view from '../images/view.svg';
import { useState } from 'react';

const Password = () => {
  const [validations,setValidations] = useState([])
  const [strength,setStrength] = useState(0)
  const [showPassword,setShowPassword] = useState(false)

  function validatePassword(e){
    setValidations(
      [
        (e.target.value.length > 5),
        (e.target.value.search(/[A-Z]/) > -1),
        (e.target.value.search(/[0-9]/) > -1),
        (e.target.value.search(/[$&+,:;=?@#]/) > -1)
      ]
    );

    setStrength(validations.reduce((acc,cur)=> acc+cur))
  }

  function handlePassword(){
    setShowPassword(!showPassword);
  }

  return (
    <form className="passwordForm">
          <label htmlFor="password">Password</label>
            <div className="passwordContainer">
              <input
              autoFocus
              className="passwordInput"
              id='password'
              type={showPassword ? 'text' : 'password'}
              required
              onChange={validatePassword}
              />
              <img src={showPassword ? view : viewEmpty} alt="Show Password" onClick={handlePassword}></img>
            </div>
            
            <p className="smallTxt">Recover password</p>
            <button
            type="submit"
            aria-label="Log In"
            className={strength >= 3 ? "btn-mobile nactive" : "btn-mobile unactive"}
            >
            Log In
            </button>
      </form>
  )
}

export default Password