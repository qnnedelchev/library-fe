import search from '../images/search.svg';
const Search = () => {
  return (
    <form className="searchForm" onSubmit={(e) => e.preventDefault()}>
        <label className="search-label" htmlFor="search">All books</label>
        <div className="search-container">
            <input
                id='search'
                type='text'
                
                placeholder="Search"
            />
            <img className="search-icon" src={search} alt="Search Icon"></img>
        </div>
    </form>
  )
}

export default Search