import books from "../images/books.png"

const SideImage = () => {
  return (
    <div className="sideImage">
        <img className="books" src={books} alt="A woman's hand picking a book from a books shelf."></img>
    </div>
  )
}

export default SideImage