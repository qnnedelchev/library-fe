import Header from './Header'

import viewEmpty from '../images/viewEmpty.png';
import view from '../images/view.svg';

import { useState, useRef, useEffect } from 'react';
import axios from '../api/axios'
import { Link, useNavigate, useLocation} from 'react-router-dom';

//CONSTS
//const USER_REGEX = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
//const PWD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{8,24}$/;

const USER_REGEX = /[a-zA-Z0-9_]{4,}$/i;
const PWD_REGEX = /([a-zA-Z0-9]){6,}$/;
const REGISTER_URL = '/api/user/register'
const BASE_URL = "https://books-library-dev.herokuapp.com";

const SignUp = ({ isMobile, signUp, setSignUp }) => {

const navigate = useNavigate();
const location = useLocation();
const from = location.state?.from?.pathname || '/';

const userRef = useRef();
const errRef = useRef();

const [user, setUser] = useState('');
const [validName, setValidName] = useState(false);
const [userFocus, setUserFocus] = useState(false);

const [pwd, setPwd] = useState('');
const [validPwd, setValidPwd] = useState(false);
const [pwdFocus, setPwdFocus] = useState(false);

const [matchPwd, setMatchPwd] = useState('');
const [validMatch, setValidMatch] = useState(false);
const [matchFocus, setMatchFocus] = useState(false);

const [showPassword,setShowPassword] = useState(false)
const [showMatch,setShowMatch] = useState(false)

const [errMsg, setErrMsg] = useState('');
const [success, setSuccess] = useState('');

useEffect(() =>{
    userRef.current.focus();
  }, [])
  
useEffect(() => {
    const result = USER_REGEX.test(user);
    console.log(result);
    console.log(user);
    setValidName(result);
}, [user])

useEffect(() => {
    const result = PWD_REGEX.test(pwd);
    console.log(result)
    console.log(pwd)
    setValidPwd(result);

    const match = (pwd === matchPwd);
    setValidMatch(match)
}, [pwd, matchPwd])

useEffect(() => {
    setErrMsg('')
  }, [user, pwd, matchPwd])


const handleSignUp = () =>{
    setSignUp(!signUp);
}

const handleSubmit = async (e) =>{
    e.preventDefault();

    const v1 = USER_REGEX.test(user);
    const v2 = PWD_REGEX.test(pwd);
    if(!v1 || !v2){
        setErrMsg("Invalid Entry");
        return;
    }
    console.log('SUBMIT')
    try {
            const postOptions = {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({username: user, password: pwd})
            };

           const response = await fetch(`https://books-library-dev.herokuapp.com/api/user/register`, postOptions);
           setUser('');
           setPwd('');
           setMatchPwd('');
           console.log(response);
           setSignUp(true);
           navigate(from, {replace: true});
    }catch (err) {
        if(!err?.response){
            setErrMsg('No Server Response');
        }else if (err.response?.status === 409){
            setErrMsg('Email address already used');
        }else {
            setErrMsg('Registration Failed');
        }
    }
   
}
 
function handlePassword(){
    setShowPassword(!showPassword);
}

function handleMatch(){
    setShowMatch(!showMatch);
}
  
return (
    <>
    {success ? (
        <section>
            <h1>Success!</h1>
        </section>) : (
    <form onSubmit={handleSubmit} className={isMobile ? "logInForm-mobile" : "logInForm-web"} >
        {isMobile ? '' : <Header isMobile={isMobile}/>}
       
        <h2 className='title-sign-up-mobile'>
            Welcome to the best book Database!
        </h2>
        <h2 className='subtitle-sign-up-mobile'>
            Create your profile
        </h2>

        <div className="emailForm">
                <label htmlFor="email">Username</label>
                <input
                className="emailInput"
                name='email'
                id='email'
                type='text'
                ref={userRef}
                required
                aria-invalid={validName ? 'false' : 'true'}
                autoComplete='off'
                value={user}
                onChange={(e) => setUser(e.target.value)}
                onFocus={() => setUserFocus(true)}
                onBlur={() => setUserFocus(false)}
                />
            </div>
    
            <div className="passwordForm">
                <label htmlFor="password">Password</label>
                <div className="passwordContainer">
                    <input
                    className="passwordInput" 
                    id='password'
                    type={showPassword ? 'text' : 'password'}
                    required
                    aria-invalid={validPwd ? 'false' : 'true'}
                    value={pwd}
                    onChange={(e) => setPwd(e.target.value)}
                    onFocus={() => setPwdFocus(true)}
                    onBlur={() => setPwdFocus(false)}
                    />
                    <img src={showPassword ? view : viewEmpty} alt="Show Password" onClick={handlePassword}></img>
                </div>
                
                <div className="confirmPasswordForm">
                    <label htmlFor="confirmPassword">Repeat Password</label>
                        <div className="passwordContainer">
                        <input
                        className="passwordInput"
                        id='confirmPassword'
                        type={showMatch ? 'text' : 'password'}
                        required
                        value={matchPwd}
                        onChange={(e) => setMatchPwd(e.target.value)}
                        onFocus={() => setMatchFocus(true)}
                        onBlur={() => setMatchFocus(false)}
                        />
                        <img src={showMatch ? view : viewEmpty} alt="Show Password" onClick={handleMatch}></img>
                        </div>
                </div>
                <button
                type="submit"
                aria-label="Sign Up"
                className={!validName || !validPwd || !validMatch ? "btn-mobile unactive" : "btn-mobile active"}
                disabled={!validName || !validPwd || !validMatch ? true : false}
                >
                Sign Up
                </button>
            </div>
            
            <footer>
                <p>You have an account? <Link to="/" ><b className='green' onClick={handleSignUp}>Log In Here</b></Link></p>
            </footer>
    
    </form> )}
</>
)
}

export default SignUp