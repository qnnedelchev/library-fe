import axios from 'axios';

export default axios.create({
    baseURL: "https://books-library-dev.herokuapp.com"
});