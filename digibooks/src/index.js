import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import AppUpdated from './AppUpdated';
import Library from './Components/Library';
import {AuthProvider} from './context/AuthProvider';
import { BrowserRouter, Routes, Route} from 'react-router-dom';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <AuthProvider>
        <Routes>
          <Route path= '/*' element={<AppUpdated />} />
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

